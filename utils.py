import glob
import torch
import numpy as np

import SVC

path = "path_to_data"
path = "/home/jonas/Documents/nn/result/clean/SVC"

def test(a, dic):
    if dic is None:
        return True
    for key in dic.keys():
        if not getattr(a, key) == dic[key]:
            return False
    return True

def load_data(name, dic):
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        with open(filename, 'rb') as f:
            a = torch.load(f, map_location="cpu")
            if test(a, dic):
                r = torch.load(f, map_location="cpu")
                return a, r

def class_data(data, dic, var):

    xs = list(set([getattr(a, var) for a in data["args"] if test(a, dic)]))
    xs.sort()

    data_ = dict()
    for x in xs:
        data_[x] = list()
        for a, r in zip(data["args"], data["res"]):
            if test(a, dic) and getattr(a, var) == x:
                data_[x].append(r)

    return data_

def get_argument(name, arg, dic=None):

    xs = list()
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        with open(filename, 'rb') as f:
            a = torch.load(f, map_location="cpu")
            if test(a, dic):
                xs.append(getattr(a, arg))
    xs = list(set(xs))
    xs.sort()

    return xs

def get_mean(data):

    ps = np.array([p for p in data.keys()])
    means = list()

    for p in ps:
        means.append(np.mean(data[p]))
    means = np.array(means)

    return ps, means

def get_std(data):

    ps = np.array([p for p in data.keys()])
    stds = list()

    for p in ps:
        stds.append(np.std(data[p]))
    stds = np.array(stds)

    return ps, stds


def sort_data(r, dalpha):

    r_ = np.array([x for x, y in zip(r, dalpha) if x > 0])
    dalpha_ = np.array([y for x, y in zip(r, dalpha) if x > 0])
    idx = np.argsort(r_)

    return r_[idx], dalpha_[idx]

def get_distribution(r, dalpha, chunk=5):

    r_ = list()
    dalpha_ = list()

    i = 0
    while True:
        dalpha_.append(dalpha[i: i+chunk].mean())
        r_.append(r[i: i+chunk].mean())
        i += chunk
        if i + chunk >= len(r):
            break

    r_ = np.array(r_)
    pdf = np.array(dalpha_) / np.sum(dalpha_)
    cdf = pdf.copy()
    for i in range(len(pdf) - 1):
        cdf[i+1] += cdf[i]

    return r_, pdf, cdf

def get_rstar(r, cdf, ratio=0.5):

    for i in range(len(cdf) - 1):
        if cdf[i] < ratio < cdf[i + 1]:
            rstar = r[i] + (ratio - cdf[i]) / (cdf[i + 1] - cdf[i]) * (r[i + 1] - r[i])
            break

    return rstar

def get_rc(name, dic, chunk=5, ratio=0.9):

    data = {"args": list(), "res": list()}
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        try:
            with open(filename, 'rb') as f:
                a = torch.load(f, map_location="cpu")
                if test(a, dic):
                    r = torch.load(f, map_location="cpu")
                    r, dalpha = sort_data(r["r"], r["dalpha"])
                    r_, pdf, cdf = get_distribution(r, dalpha, chunk)
                    rstar = get_rstar(r_, cdf, ratio)
                    data["res"].append(rstar)
                    data["args"].append(a)
        except:
            pass
    return data

def get_alpha(name, dic):

    data = {"args": list(), "res": list()}
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        try:
            with open(filename, 'rb') as f:
                a = torch.load(f, map_location="cpu")
                if test(a, dic):
                    r = torch.load(f, map_location="cpu")
                    data["res"].append(np.abs(r["clf"].dual_coef_[0]).mean())
                    data["args"].append(a)
        except:
            pass
    return data

def get_Delta(name, dic):

    data = {"args": list(), "res": list()}
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        try:
            with open(filename, 'rb') as f:
                a = torch.load(f, map_location="cpu")
                if test(a, dic):
                    r = torch.load(f, map_location="cpu")
                    indices = r["clf"].support_
                    xtr, ytr = SVC.get_data(a, a.ptr, a.seed)
                    sv = xtr[:, indices]
                    if a.dataset == "interface":
                        delta = np.abs(sv[0]).mean()
                    elif a.dataset == "stripe":
                        if a.xlim is None:
                            xmin, xmax = -0.3, 1.18549
                        else:
                            xmin, xmax = a.xlim
                        delta = np.min((np.abs(sv[0] - xmin), np.abs(sv[0] - xmax)), axis=0).mean()
                    elif a.dataset == "sphere":
                        delta = np.abs(np.linalg.norm(sv, axis=0) - a.d**0.5).mean()
                    else:
                        pass
                    data["res"].append(delta)
                    data["args"].append(a)
        except:
            pass
    return data

def get_test_error(name, dic):

    data = {"args": list(), "res": list()}
    for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
        try:
            with open(filename, 'rb') as f:
                a = torch.load(f, map_location="cpu")
                if test(a, dic):
                    r = torch.load(f, map_location="cpu")
                    data["res"].append(r["err"])
                    data["args"].append(a)
        except:
            pass
    return data

def get_Q2(name, d):

    dic = {"d": d}
    ps = get_argument(name, "ptr", dic)

    data = {"k": dict(), "Q2": dict()}
    for p in ps:
        dic["ptr"] = p
        for filename in glob.glob("{}/{}/*.pkl".format(path, name)):
            with open(filename, 'rb') as f:
                a = torch.load(f, map_location="cpu")
                if test(a, dic):
                    data["k"][p], data["Q2"][p], _ = torch.load(f, map_location="cpu")

    return data
