import os
import pickle
import argparse
import torch
import time

import numpy as np
from scipy.spatial.distance import cdist
from sklearn import svm

def random_data(args, seed):

    np.random.seed(seed)
    x = np.random.randn(args.d, args.p)
    if args.dataset == "interface":
        y = 2 * (x[0, :] > 0) - 1
    elif args.dataset == "sphere":
        norm = np.linalg.norm(x, axis=0)
        y = 2 * (norm > args.d**0.5) - 1
    elif args.dataset == "stripe":
        a, b = -0.3, 1.18549
        y = 2 * ((x[0, :] > a) * (x[0, :] < b)) - 1
    else:
        print("invalid dataset")

    return x, y

def generate_data(args, seed, xref):

    x1, y1 = random_data(args, seed)
    x1[:, 0] = xref
    y1[0] = 1

    x2, y2 = random_data(args, seed)
    x2 = np.delete(x2, 0, axis=1)
    y2 = np.delete(y2, 0, axis=0)

    return (x1, y1), (x2, y2)

def get_xref(args):

    np.random.seed(args.ref_seed)
    if args.dataset == "interface":
        xref = np.random.randn(args.d)
        xref[0] = args.eps
    if args.dataset == "stripe":
        a = -0.3
        xref = np.random.randn(args.d)
        xref[0] = a + args.eps
    if args.dataset == "sphere":
        xref = np.random.randn(args.d)
        norm = np.linalg.norm(xref)
        xref = xref / norm * args.d**0.5 * (1 + args.eps / args.d**0.5)

    return xref

def kernel(args):

    if args.kernel == "laplace":
        def ker(x, y):
            return np.exp(-np.linalg.norm(x - y) / args.sigma)
    elif args.kernel == "gaussian":
        def ker(x, y):
            return np.exp(-0.5 * (np.linalg.norm(x - y) / args.sigma)**2)
    elif args.kernel == "NN":
        ker = NNkernel(args)
    elif args.kernel == "matern":
        from sklearn.gaussian_process.kernels import Matern
        ker = Matern(length_scale=args.sigma, nu=args.nu)
    else:
        print("invalid kernel")

    return ker

def get_sv(ker, x, y):

    K = cdist(x.transpose(), x.transpose(), ker)
    clf = svm.SVC(C=1e20, kernel="precomputed", max_iter=-1)
    clf.fit(K, y)

    return x[:, clf.support_], clf.dual_coef_[0]

def reshape_sv(sv1, sv2, alpha1, alpha2):

    s21 = (set(sv2[0]) - set(sv1[0]))
    s12 = (set(sv1[0]) - set(sv2[0]))

    i = 0
    for sv, alpha in zip(sv1.transpose(), alpha1):
        if sv[0] in s12:
            sv2 = np.append(sv2, np.array([sv]).transpose(), axis=1)
            alpha2 = np.append(alpha2, 0.)
            sv1 = np.append(sv1, np.array([sv]).transpose(), axis=1)
            sv1 = np.delete(sv1, i, axis=1)
            alpha1 = np.append(alpha1, alpha)
            alpha1 = np.delete(alpha1, i)
            i -= 1
        i += 1

    i = 0
    for sv, alpha in zip(sv2.transpose(), alpha2):
        if sv[0] in s21:
            sv1 = np.insert(sv1, i, sv, axis=1)
            alpha1 = np.insert(alpha1, i, 0.)
        i += 1

    return (sv1, sv2), (alpha1, alpha2)

def run(args):

    xref = get_xref(args)
    ker = kernel(args)
    n = 0
    seed = 1000 * args.data_seed
    while n < args.N:
        (x1, y1), (x2, y2) = generate_data(args, seed, xref)
        sv1, alpha1 = get_sv(ker, x1, y1)
        sv2, alpha2 = get_sv(ker, x2, y2)
        (sv1, sv2), (alpha1, alpha2) = reshape_sv(sv1, sv2, alpha1, alpha2)
        _dalpha = alpha1 - alpha2
        _r = cdist([xref], sv1.transpose())[0]
        if n == 0:
            r = _r
            dalpha = _dalpha
        else:
            r = np.append(r, _r)
            dalpha = np.append(dalpha, _dalpha)
        n += len(_r)
        seed += 1

    res = {
        "r": r,
        "dalpha": dalpha
        }

    return res

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset", type=str, required=True, help="interface, stripe, sphere")
    parser.add_argument("--d", type=int, required=True)
    parser.add_argument("--p", type=int, required=True)
    parser.add_argument("--data_seed", type=int, default=0)
    parser.add_argument("--ref_seed", type=int, default=0)
    parser.add_argument("--eps", type=float, required=True)
    parser.add_argument("--N", type=int, default=500)

    parser.add_argument("--kernel", type=str, required=True, default="laplace, gaussian, matern, NN")
    parser.add_argument("--sigma", type=float, required=True)
    parser.add_argument("--nu", type=float, default=None)
    parser.add_argument("--C", type=float, default=1e20)

    parser.add_argument("--pickle", type=str, required=True)
    args = parser.parse_args()

    torch.save(args, args.pickle)
    try:
        res = run(args)
        with open(args.pickle, 'wb') as f:
            torch.save(args, f)
            torch.save(res, f)
    except:
        os.remove(args.pickle)
        raise

if __name__ == "__main__":
    main()
