import os
import sys
import pickle
import argparse
import torch
import time
import signal

import numpy as np
from scipy.spatial.distance import cdist
from scipy.special import gamma
from scipy.special import erf, erfinv
from sklearn.svm import SVC

class TimeoutException(Exception):   # Custom exception class
    pass

def timeout_handler(signum, frame):   # Custom signal handler
    raise TimeoutException

def get_data(args, p, seed):

    np.random.seed(seed)

    if args.dist == "normal":
        x = np.random.randn(args.d, p)
    elif args.dist == "uniform":
        x = (np.random.rand(args.d, p) - 0.5)
    else:
        print("invalid distribution")

    if args.dataset == "interface":
        y = 2 * (x[0, :] > 0) - 1
        if args.Delta0 > 0:
            x[0] = x[0] + np.sign(x[0]) * args.Delta0 / 2
    elif args.dataset == "sphere":
        norm = np.linalg.norm(x, axis=0)
        y = 2 * (norm > args.d**0.5) - 1
    elif args.dataset == "stripe":
        if args.xlim is None:
            a, b = -0.3, 1.18549
        else:
            a, b = args.xlim
        y = 2 * ((x[0, :] > a) * (x[0, :] < b)) - 1
    else:
        print("invalid dataset")

    return x, y

def kernel(args):

    if args.kernel == "laplace":
        def ker(x, y):
            return np.exp(-np.linalg.norm(x - y) / args.sigma)
    elif args.kernel == "gaussian":
        def ker(x, y):
            return np.exp(-0.5 * (np.linalg.norm(x - y) / args.sigma)**2)
    elif args.kernel == "matern":
        from sklearn.gaussian_process.kernels import Matern
        ker = Matern(length_scale=args.sigma, nu=args.nu)
    else:
        print("invalid kernel")

    return ker

def func(args, clf, xtr, xte, ker):

    d, pte = xte.shape
    d, ptr = xtr.shape

    indices = clf.support_
    if args.kernel == "laplace" or args.kernel == "gaussian":
        vec = cdist(xtr[:, indices].transpose(), xte.transpose(), ker)
    elif args.kernel == "matern":
        vec = ker(xtr[:, indices].transpose(), xte.transpose())

    f = np.zeros(pte)
    for i in range(len(indices)):
        f += clf.dual_coef_[0][i] * vec[i]
    if args.bias:
        f += clf.intercept_

    return f

def gram_matrix(args, x1, x2, ker):

    if args.kernel == "laplace" or args.kernel == "gaussian":
        k = cdist(x1.transpose(), x2.transpose(), ker)
    if or args.kernel == "matern":
        k = ker(x1.transpose(), x2.transpose())

    return k

def stretching(args, x):

    if args.lam is None:
        return x
    elif type(args.lam) == dict:
        x[1:, :] /= args.lam[args.ptr]
    else:
        x[1:, :] /= args.lam

    return x

def run(args):

    tic = time.time()
    xtr, ytr = get_data(args, args.ptr, args.seed)
    xtr = stretching(args, xtr)
    ker = kernel(args)
    Ktrtr = gram_matrix(args, xtr, xtr, ker)
    clf = SVC(C=args.C, kernel="precomputed", max_iter=-1)
    clf.fit(Ktrtr, ytr)
    tac = time.time()

    res = {
        "clf": clf,
        "wall": round(tac - tic, 2),
        "err": None
    }

    if args.test:
        n_tot = 0
        n_err = 0
        seed = 10 * args.seed * args.ptr
        while True:
            xte, yte = get_data(args, 1000, seed)
            xte = stretching(args, xte)
            ote = func(args, clf, xtr, xte, ker)
            n_err += (ote * yte <= 0).sum()
            n_tot += 1000
            seed += 1
            if n_tot > args.pte and n_err > args.n_err:
                break
        res["err"] = n_err / n_tot

    return res

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset", type=str, required=True, help="interface, sphere, stripe")
    parser.add_argument("--dist", type=str, required=True, help="normal, uniform")
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--ptr", type=int, required=True)
    parser.add_argument("--d", type=int, required=True)
    parser.add_argument("--xlim", nargs=2, type=float, default=None)
    parser.add_argument("--Delta0", type=float, default=0)
    parser.add_argument("--lam", nargs="*", type=float, default=None)

    parser.add_argument("--test", type=int, default=1)
    parser.add_argument("--pte", type=int, default=1000)
    parser.add_argument("--n_err", type=int, default=10)

    parser.add_argument("--kernel", type=str, required=True, help="laplace, gaussian, matern")
    parser.add_argument("--sigma", type=float, required=True)
    parser.add_argument("--nu", type=float, default=None)
    parser.add_argument("--C", type=float, default=1e20)
    parser.add_argument("--bias", type=int, default=1)

    parser.add_argument("--max_time", type=int, default=10000)
    parser.add_argument("--pickle", type=str, required=True)
    args = parser.parse_args()

    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(args.max_time)

    torch.save(args, args.pickle)
    try:
        res = run(args)
        with open(args.pickle, 'wb') as f:
            torch.save(args, f)
            torch.save(res, f)
    except TimeoutException:
        print("Timeout")
        os.remove(args.pickle)
        raise
    except:
        os.remove(args.pickle)
        raise


if __name__ == "__main__":
    main()
