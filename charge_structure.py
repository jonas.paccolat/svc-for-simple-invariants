import os
import pickle
import glob
import argparse
import torch
import time

import numpy as np
import SVC
import utils

def extract(args, a, r):

    clf = r["clf"]
    xtr, ytr = SVC.get_data(a, a.ptr, a.seed)

    return xtr[:, clf.support_], clf.dual_coef_[0]

def wave_vector(args):

    if args.scale == "lin":
        kperp = np.linspace(args.kmin, args.kmax, args.Nk)
    elif args.scale == "log":
        kperp = np.array([10**x for x in np.linspace(args.kmin, args.kmax, args.Nk)])
    else:
        print("invalid scale.")

    return kperp

def spectrum(args, sv, alpha):

    k = np.zeros(args.d)
    kperp = wave_vector(args)

    rho1 = list()
    rho2 = list()
    for kperp_ in kperp:
        rho_1 = list()
        rho_2 = list()
        for i in range(args.Niter):
            n = np.random.randn(args.d - 1)
            n /= (n**2).sum()**0.5
            k[1:] = kperp_ * n
            real1 = np.array([np.cos(np.dot(k, sv[:, mu])) * alpha[mu] for mu in range(len(alpha))]).sum()
            imag1 = np.array([np.sin(np.dot(k, sv[:, mu])) * alpha[mu] for mu in range(len(alpha))]).sum()
            real2 = np.array([np.cos(np.dot(k, sv[:, mu])) * np.sign(sv[0, mu]) for mu in range(len(alpha))]).sum() * np.mean(np.abs(alpha))
            imag2 = np.array([np.sin(np.dot(k, sv[:, mu])) * np.sign(sv[0, mu]) for mu in range(len(alpha))]).sum() * np.mean(np.abs(alpha))
            rho_1.append(real1**2 + imag1**2)
            rho_2.append(real2**2 + imag2**2)
        rho1.append(np.mean(rho_1))
        rho2.append(np.mean(rho_2))
    rho1 = np.array(rho1)
    rho2 = np.array(rho2)

    return kperp, rho1, rho2

def run(args):

    dic = {"ptr": args.ptr, "sigma": args.sigma, "d": args.d}
    a, r = utils.load_data(args.name, dic)
    sv, alpha = extract(args, a, r)

    return spectrum(args, sv, alpha)

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--name", type=str, required=True)
    parser.add_argument("--ptr", type=int, required=True)
    parser.add_argument("--d", type=int, required=True)
    parser.add_argument("--sigma", type=float, default=1e2)
    parser.add_argument("--Niter", type=int, default=100)

    parser.add_argument("--scale", type=str, required=True)
    parser.add_argument("--kmin", type=float, required=True)
    parser.add_argument("--kmax", type=float, required=True)
    parser.add_argument("--Nk", type=int, default=100)

    parser.add_argument("--pickle", type=str, required=True)
    args = parser.parse_args()

    torch.save(args, args.pickle)
    try:
        res = run(args)
        with open(args.pickle, 'wb') as f:
            torch.save(args, f)
            torch.save(res, f)
    except:
        os.remove(args.pickle)
        raise

if __name__ == "__main__":
    main()
